<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Requests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Request'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>

  <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
              'label'=>'Departure Airport',
              'value'=>'from.airport_tr',
            ],
            [
              'label'=>'Destination Airport',
              'value'=>'to.airport_tr',
            ],
            'arrival_date',

            //'description',
            // 'user_id',
            // 'carriage_id',

            ['class' => 'yii\grid\ActionColumn',
            'template'=>'{inspect}',
                              'buttons'=>[
                                'inspect' => function ($url, $model) {
                                  return Html::a('<span class="glyphicon glyphicon-search inspect" data-id="'.$model->id.'" data-url="/requests/view?id='.$model->id.'" data-toggle="modal" data-target="#inspect-model"></span>', ['#'.$model->id], [
                                          'title' => Yii::t('yii', 'View'),
                                  ]);

                                }
                            ]
                          ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>


<!-- Modal -->
<div class="modal fade" id="inspect-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="container-fluid modal-body">

      </div>

    </div>
  </div>
</div>
