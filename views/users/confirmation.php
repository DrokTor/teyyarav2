<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Account validation';
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (isset($erroruser)): ?>
      <?= Yii::t('app','This email seems to be already registered.') ?>

    <?php elseif (isset($errortemp)): ?>
      <?= Yii::t('app','This email seems to be already registered but the account was not activated.') ?>
    <?php else: ?>
    <p>
        <?= Yii::t('app','An email has been sent to the address given. Please confirm your email to activate your account.') ?>
    </p>
    <?php endif; ?>


</div>
