<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Password reset';
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (isset($expired)): ?>
      <p> <?= Yii::t('app', 'The link seems to have expired.'); ?></p>

    <?php elseif (isset($reset)): ?>
      <p> <?= Yii::t('app', 'Your password has been rest successfully.'); ?></p>

    <?php else: ?>
        <?= Html::beginForm(['resetvalid'], 'post', ['optionName' => 'optionValue']); ?>
          <?= Html::passwordInput('password', '', ['placeholder' => 'enter your new password']); ?>
          <?= Html::hiddenInput('token', $hash); ?>
         <?= Html::submitButton('Reset my password', ['optionName' => 'optionValue']); ?>
        <?= Html::endForm(); ?>
    <?php endif; ?>


</div>
