<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Password reset';
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (isset($notfound)): ?>
      <p> <?= Yii::t('app', 'This email address is not registered.'); ?></p>

    <?php elseif (isset($confirmation)): ?>
      <p> <?= Yii::t('app', 'A reset link has been sent to your email address.'); ?></p>

    <?php else: ?>
        <?= Html::beginForm(['reset'], 'post', ['optionName' => 'optionValue']); ?>
         <?= Html::textInput('email', '', ['placeholder' => 'enter your email address']); ?>
         <?= Html::submitButton('Send reset link', ['optionName' => 'optionValue']); ?>
        <?= Html::endForm(); ?>
    <?php endif; ?>


</div>
