<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Account validation';
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (isset($exist)): ?>
      <?= Yii::t('app','This email seems to be already registered.') ?>

    <?php elseif ($first_name): ?>
      <?= Yii::t('app','Welcome <strong> {name} </strong> to the TEYYARA community !',['name'=>$first_name]) ?>
        <br>
        <br>
        <a href="<?= Url::to('/site/login'); ?>" >You can now login</a>
    <?php elseif(isset($error)): ?>
    <p>
        <?= Yii::t('app','An error seems to have occured when validating the account. Please contact our') ?>
        <a href="<?= Url::to('/site/contact'); ?>" > support. </a>
    </p>
    <?php else: ?>
    <p>
        <?= Yii::t('app','The link seems to have expired. For more details, please contact our') ?>
        <a href="<?= Url::to('/site/contact'); ?>" > support. </a>
    </p>
    <?php endif; ?>



</div>
