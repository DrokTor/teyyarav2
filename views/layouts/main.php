<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url ;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php Pjax::begin(['id'=>'pjax-main','timeout'=>5000,'options'=>['style'=> 'height:100%']]); ?>
<?php //Pjax::begin(['timeout' => 5000 ]); ?>


    <?php
    NavBar::begin([
        'brandLabel' => '<img class="col-md-10 img-responsive" src="/images/logo_200.png"/>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
            'style'=> (Url::current()=='/site/index' || Url::current()=='/')? "display:none":"",

        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Post', 'items'=>[
              ['label' => 'Offer', 'url' => ['/offers/create']],
              ['label' => 'Request', 'url' => ['/requests/create']],

            ]],
            ['label' => 'Search',   'items'=>[
              ['label' => 'Offer', 'url' => ['/offers/index']],
              ['label' => 'Request', 'url' => ['/requests/index']],

            ]],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->first_name . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
<div class="wrap">
    <div id="main-content" class="container-fluid col-md-10 col-md-offset-1"    <?php   if(Url::current()!='/site/index' && Url::current()!='/') echo 'style="display:none;height:200%;"'; else echo 'style="height:100%"'; ?> >
        <?php /* Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) */ ?>
        <?= $content ?>
    </div>
</div>
<?php Pjax::end() ; ?>

<!--<div class="container">
<footer class="footer">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
</footer>
</div>-->

<div id="loading-container" class="collapse">
</div>
<?php $this->endBody() ?>
</body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.12/URI.min.js">
</script>
</html>
<?php $this->endPage() ?>
