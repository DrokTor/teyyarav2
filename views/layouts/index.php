<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url ;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php Pjax::begin(['id'=>'pjax-main','timeout'=>5000,'options'=>['style'=> 'height:100%']]); ?>
<?php //Pjax::begin(['timeout' => 5000 ]); ?>

<div class="wrap-index">


    <div id="index-content" class="container-fluid" style="height:100%" >
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>
<?php Pjax::end() ; ?>


<?php $this->endBody() ?>
</body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.12/URI.min.js">
</script>
</html>
<?php $this->endPage() ?>
