<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Travel */

$this->title = Yii::t('app', 'Create Travel');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Travels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="travel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'carriage'=>$carriage,
        'types'=>$types,
        'countryModel'=>$countryModel,
        'countries'=>$countries,
        'currencies'=>$currencies,
        'weights'=>$weights
    ]) ?>

</div>
