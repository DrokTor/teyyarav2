<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Travels');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="travel-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Travel'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id_travel',
            [
              'label'=>'Departure Airport',
              'value'=>'from.airport_tr',
            ],
            'departure_date',
            //'departure_time',
            [
              'label'=>'Destination Airport',
              'value'=>'to.airport_tr',
            ],
            // 'arrival_date',
            // 'arrival_time',
            // 'type',
            // 'id_carriage',
            // 'id_user',

            ['class' => 'yii\grid\ActionColumn',
            'template'=>'{inspect}',
                              'buttons'=>[
                                'inspect' => function ($url, $model) {
                                  return Html::a('<span class="glyphicon glyphicon-search inspect" data-id="'.$model->id_travel.'" data-url="/offers/view?id='.$model->id_travel.'" data-toggle="modal" data-target="#inspect-model"></span>', ['#'.$model->id_travel], [
                                          'title' => Yii::t('yii', 'View'),
                                  ]);

                                }
                            ]

          ],
        ],
    ]); ?>
 </div>



 <!-- Modal -->
 <div class="modal fade" id="inspect-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
 <div class="modal-dialog" role="document">
 <div class="modal-content">
 <div class="modal-header">
 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
 <h4 class="modal-title" id="myModalLabel">Modal title</h4>
 </div>
 <div class="container-fluid modal-body">

 </div>

 </div>
 </div>
 </div>
