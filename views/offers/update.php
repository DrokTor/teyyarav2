<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Travel */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Travel',
]) . $model->id_travel;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Travels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_travel, 'url' => ['view', 'id' => $model->id_travel]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="travel-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
