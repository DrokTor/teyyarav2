<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use kartik\time\TimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= Html::activeDropDownList( $countryModel,'id_country',$countries,['prompt'=>'', 'class'=>'from_country' ]) ?>


    <?= $form->field($model,'id_from')->dropDownList([],['prompt'=>'select country first', 'class'=>'form-control from_airport'] ) ?>


    <?= Html::activeDropDownList( $countryModel,'id_country',$countries,['prompt'=>'', 'class'=>'to_country' ]) ?>

    <?= $form->field($model,'id_to')->dropDownList([],['prompt'=>'select country first', 'class'=>'form-control to_airport'] ) ?>



    <?php // $form->field($model, 'arrival_date')->textInput() ?>

    <?= $form->field($model, 'departure_date')->widget(
    DatePicker::className(), [
        // inline too, not bad
         //'inline' => true,
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{addon}{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <?=  $form->field($model, 'departure_time')->widget(TimePicker::classname(), []); ?>

    <?= $form->field($model, 'arrival_date')->widget(
    DatePicker::className(), [
        // inline too, not bad
         //'inline' => true,
         // modify template for custom rendering
        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{addon}{input}</div>',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
    ]);?>

    <?=  $form->field($model, 'arrival_time')->widget(TimePicker::classname(), []); ?>


    <?php /* $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'carriage_id')->textInput() */?>

    <?= $form->field($carriage,'id_type')->dropDownList($types,['prompt'=>'select carriage type', 'class'=>'form-control '] ) ?>

    <?= $form->field($carriage,'id_weight')->dropDownList($weights,['prompt'=>'select weight', 'class'=>'form-control '] ) ?>

    <?= $form->field($carriage, 'price')->textInput() ?>

    <?= $form->field($carriage,'id_currency')->dropDownList($currencies,['prompt'=>'select currency', 'class'=>'form-control '] ) ?>

    <?= $form->field($carriage, 'comment')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
