<?php

/* @var $this yii\web\View */

$this->title = 'TEYYARA';
?>
<div class="index-page">

<div class="header-index">
  <img src="/images/logo_500.png"/>

</div>
<div class="row airport-img">

<img src="/images/airport.jpg"/>
  <div class="welcome-msg">
  <div class="actions collapse">
  <a class="ajax-btn btn btn-primary" href='/offers/search'>
    Search <i class="glyphicon glyphicon-search"></i>
  </a >
  <div class="ajax-btn btn btn-primary" data-url='/offers/post'>
    Publish <i class="glyphicon glyphicon-plane"></i>
  </div>
  </div>
  <div class="messages">

  <div class=" text-1 collapse">
  Want to send something abroad ?
</div>
<div class=" text-2 collapse">
  Looking for someone who can transport it for you ?
</div>
<div class=" text-3 collapse">
  TEYYARA is the plateform you're looking for !
</div>
<button class="howitworks collapse btn btn-default" type="button" name="button">See how it works <i class="glyphicon glyphicon-chevron-down"></i></button>
</div>

</div>
<img class="clouds" src="/images/clouds.png"/>
<!--<img src="/images/demo.png"/>-->
<div class="demo  ">
  <img class="collapse" src="/images/sender.png"/>
  <img class="collapse" src="/images/document.png"/>
  <img class="collapse" src="/images/transporter.png"/>
  <img class="collapse airplane" src="/images/airplane.png"/>
  <img class="collapse" src="/images/transporter.png"/>
  <img class="collapse" src="/images/document.png"/>
  <img class="collapse" src="/images/sender.png"/>


</div>
</div>
</div>
