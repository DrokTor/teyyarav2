<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\UserTemp;
use app\models\Reset;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //dd( Yii::$app->request->post());
        //dd( Yii::$app->user);
        $model = new User();
        $post=Yii::$app->request->post();
        if($post){


          $checkUser= User::findOne(['email'=>$post['User']['email']]);
          $checkUserTemp= UserTemp::findOne(['email'=>$post['User']['email']]);
          //dd($checkUser);
          if($checkUserTemp || $checkUser){
           return $this->render('confirmation',['erroruser'=>$checkUser,'errortemp'=>$checkUserTemp]);
          }
        }
        if ($model->load($post) && $model->validate()) {
            $temp = new UserTemp();
            $temp->attributes=$post['User'];
            //dd($temp);

            if(/*$temp->load($post) &&*/ $temp->save())
            {
              Yii::$app->mailer->compose()
             ->setFrom('support@teyyara.com')
             ->setTo($temp->email)
             ->setSubject('TEYYARA - Account validation')
             ->setHtmlBody('<h1>Thank you for using TEYYARA</h1> <a href="'.yii::$app->request->hostinfo.'/users/validate?r='.$temp->rand_temp.'" style="background-color: #008069;padding: 10px;border-radius: 2px;color: white;text-decoration: none;">Confirm my email address</a><br><br><br><hr><p style="font-size: smaller;">'.\Yii::t('app',"This email has been sent to you after your request for registration in Teyyara platform,
     				if you didn't attempt to register and you don't know how you recieved this email, you can just ignore it and delete it. ").'</p>')
             ->send();

           return $this->render('confirmation'/*['confirmation'/*, 'id' => $temp->id_user]*/);
            }
            else {
              dd($temp->errors);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionValidate($r)
    {
      $userTemp= UserTemp::findOne(['rand_temp'=>$r]);
      $checkUser= User::findOne(['email'=>$userTemp->email]);
      //dd($userTemp->email);

      if($checkUser){
        //dd($checkUser);
        return $this->render('validation',["exist"=>true]);
      }

      if($userTemp){
        $user= new User();
        $user->attributes=$userTemp->attributes;
        //dd($user);
        $user->scenario=User::SCENARIO_VALIDATION;
        if($user->save()){
          $userTemp->delete();
        }
        else dd($user->errors);
      }
      $first_name=isset($user->first_name)?$user->first_name:null;
      return $this->render('validation',["first_name"=>$first_name]);
    }
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_user]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Reset password for an existing User model.
     * @param integer $id
     * @return mixed
     */
    public function actionReset()
    {
        $post=Yii::$app->request->post();
        $confirmation=null;
        if($post){
          $user= User::findOne(['email'=>$post['email']]);
          //dd($post);
          if($user){


          $reset= new Reset();
          $reset->email=$post['email'];
          $reset->hash= Yii::$app->getSecurity()->generateRandomString();
          if($reset->save()){
            Yii::$app->mailer->compose()
           ->setFrom('support@teyyara.com')
           ->setTo($reset->email)
           ->setSubject('TEYYARA - Password reset')
           ->setHtmlBody('<h3>Click on the link below to reset your password</h3> <a href="'.yii::$app->request->hostinfo.'/users/resetvalid?r='.$reset->hash.'" style="background-color: #008069;padding: 10px;border-radius: 2px;color: white;text-decoration: none;">Reset my password</a><br><br><br><hr><p style="font-size: smaller;">'.\Yii::t('app',"This email has been sent to you after your request for password reset in Teyyara platform,
          if you didn't attempt to reset your password and you don't know how you recieved this email, you can just ignore it and delete it. ").'</p>')
           ->send();
           $confirmation=true;
           }
         } else $notfound=true;


        }
        return $this->render('reset',['confirmation'=>$confirmation, 'notfound'=>$notfound ?? null]);
    }

    public function actionResetvalid($r=null)
    {
      if(Yii::$app->request->isGet)
      {
        $reset= Reset::findOne(['hash'=>$r]);
        if(!$reset)  $expired=true;
        else $expired=null;
        return $this->render('resetvalid',['hash'=>$r,'expired'=>$expired]);
      }
      elseif(Yii::$app->request->isPost)
      {
        $post=Yii::$app->request->post();
        $reset= Reset::findOne(['hash'=>$post['token']]);
        if($reset){
          $user= User::findOne(['email'=>$reset->email]);
          $user->password= $post['password'];
          $user->scenario= User::SCENARIO_VALIDATION;
          if($user->save())
          {
            return $this->render('resetvalid',['reset'=>true]);
          }

        }
        else return $this->render('resetvalid',['expired'=>true]);

      }



    }
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
