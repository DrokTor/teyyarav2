<?php

namespace app\controllers;

use Yii;
use app\models\Request;
use app\models\Country;
use app\models\Type;
use app\models\Carriage;
use app\models\Currency;
use app\models\Airport;
use app\models\Weight;
use app\models\RequestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * RequestController implements the CRUD actions for Request model.
 */
class RequestsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Request model.
     * @param integer $id
     * @return mixed
     */
    public function actionView()
    {
        if(Yii::$app->request->isAjax)
        {
            $post= Yii::$app->request->post();
            if($post){
              $request=Request::find()->with('user','from','to','carriage')->where(['id'=>$post['id']])->one();
              //dd($request);
              //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
              $digest="<div>";
              $digest.="<div class='row'><div  class='col-md-6'><label class='col-md-4' >From</label><p class='col-md-8' >".$request->from->airport_tr."</p>";
              $digest.="<label class='col-md-4' >To</label><p class='col-md-8' >".$request->to->airport_tr."</p>";
              $digest.="<label class='col-md-4' >Date</label><p class='col-md-8' >".$request->arrival_date."</p></div>";
              $digest.="<div class='col-md-6'><label class='col-md-4' >Type</label><p class='col-md-8' >".$request->carriage->type->name_type."</p>";
              $digest.="<label class='col-md-4' >Weight</label><p class='col-md-8' >".$request->carriage->weight->weight."</p>";
              $digest.="<label class='col-md-4' >Price</label><p class='col-md-8' >".$request->carriage->price.' '.$request->carriage->currency->iso."</p>";
              $digest.="<label class='col-md-4' >Comment</label><p class='col-md-8' >".$request->carriage->comment."</p></div></div><hr>";
              $digest.="<div class='row'><div class='col-md-6'><label class='col-md-4' >First name</label><p class='col-md-8' >".$request->user->first_name."</p>";
              $digest.="<label class='col-md-4' >Last name</label><p class='col-md-8' >".$request->user->last_name."</p>";
              $digest.="<label class='col-md-4' >Phone</label><p class='col-md-8' >".$request->user->phone."</p>";
              $digest.="</div><div class='col-md-6' ><label class='col-md-4' >Email</label><p class='col-md-8' >".$request->user->email."</p>";
              $digest.="<label class='col-md-4' >Address</label><p class='col-md-8' >".$request->user->address."</p></div></div>";
              $digest.="</div>";
              return $digest;
            }
        }

    }

    /**
     * Creates a new Request model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $post= Yii::$app->request->post();
        //dd($post);
        $model = new Request();
        $carriage = new Carriage();

        $types = Type::find()->All();
        $weights = Weight::find()->All();
        $currencies = Currency::find()->All();
        $countries = Country::find()->All();
        $countryModel=new Country();

        $types= ArrayHelper::map($types,'id_type','name_tr_type');
        $countries= ArrayHelper::map($countries,'id_country','country_tr');
        $currencies= ArrayHelper::map($currencies,'id_currency','iso');
        $weights= ArrayHelper::map($weights,'id_weight','weight');

        if($post){
          if($carriage->load($post) && $carriage->save()){
            $model->user_id=28;
            $model->carriage_id=$carriage->id_carriage;
          }
          //dd($carriage->errors);

        }
        if ($model->load($post) && $model->save()) {
          return $this->redirect(['view', 'id' => $model->id]);


        }elseif($model->errors){
          dd($model->errors);
        }
         else {
            return $this->render('create', [
                'model' => $model,
                'carriage'=>$carriage,
                'types'=>$types,
                'countryModel'=>$countryModel,
                'countries'=>$countries,
                'currencies'=>$currencies,
                'weights'=>$weights
            ]);
        }
    }

    /**
     * Updates an existing Request model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Request model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Request model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Request the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAirports()
    {
      if(Yii::$app->request->isAjax)
      {
          $post=Yii::$app->request->post();
          $id=$post['id'];
          $airports= Airport::find()->where(['id_country'=>$id])->asArray()->all();
          //dd($airports);
          $list='<select>';
          foreach ($airports as $key => $value) {
            $list.='<option value="'.$value['id_airport'].'">'.$value['airport_tr'].'</option>';
          }
          $list.='</select>';
          return $list;
      }
    }
}
