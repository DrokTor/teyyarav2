CREATE DATABASE  IF NOT EXISTS `teyyara` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `teyyara`;
-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: teyyara
-- ------------------------------------------------------
-- Server version	5.5.57-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `airports`
--

DROP TABLE IF EXISTS `airports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airports` (
  `id_airport` smallint(6) NOT NULL AUTO_INCREMENT,
  `id_country` tinyint(4) NOT NULL,
  `airport` varchar(71) COLLATE utf8_unicode_ci DEFAULT NULL,
  `airport_tr` varchar(65) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_airport`),
  KEY `id_country` (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=520 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airports`
--

LOCK TABLES `airports` WRITE;
/*!40000 ALTER TABLE `airports` DISABLE KEYS */;
INSERT INTO `airports` VALUES (1,1,'Aéroport international de Kaboul','Kabul International Airport'),(2,1,'Aéroport international de Kandahar','Kandahar International Airport'),(3,1,'Aéroport international de Mazar-e-Charif','Mazar-e Sharif International Airport'),(4,1,'Aéroport international d\'Hérat','Herat International Airport'),(5,2,'Aéroport international de Cap Town','Cap town international airport'),(6,2,'Aéroport international de King Shaka','King Shaka international airport'),(7,2,'Aéroport international de Lanseria','Lanseria international airport'),(8,2,'Aéroport international de O. R. Tambo','O. R. Tambo international airport'),(9,2,'Aéroport international de Kruger Mpumalanga','Kruger Mpumalanga international airport'),(10,2,'Aéroport international de Polokwane','Polokwane International Airport'),(11,2,'Aéroport international de Upington','Upington International Airport'),(12,3,'Aéroport Alger-Houari Boumediene','Houari Boumediene-Algiers Airport'),(13,3,'Aéroport Béjaia-Soummam Abane Ramdane','Bejaia-Soummam Abane Ramdane Airport'),(14,3,'Aéroport Ghardaïa-Moufdi Zakaria','Ghardaïa-Moufdi Zakaria Airport'),(15,3,'Aéroport Hassi Messaoud-Oued Irara Krim Belkacem','Hassi Messaoud-Oued Irara–Krim Belkacem Airport'),(16,3,'Aéroport Chlef-Aboubakr Belkaid','Chlef-Aboubakr Belkaid Airport'),(17,3,'Aéroport In Amenas-Zarzaitine','In Amenas-Zarzaitine Airport'),(18,3,'Aéroport Djanet-Tiska','Djanet-Tiska Airport'),(19,3,'Aéroport Tamanrasset-Aguenar Hadj Bey Akhamokh','Tamanrasset-Aguenar Hadj Bey Akhamokh Airport'),(20,3,'Aéroport Oran-Ahmed Ben Bella','Oran-Ahmed Ben Bella Airport'),(21,3,'Aéroport Tlemcen-Zenata Messali El Hadj','Tlemcen-Zenata Messali El Hadj Airport'),(22,3,'Aéroport Annaba-Rabah Bitat','Annaba-Rabah Bitat Airport'),(23,3,'Aéroport Constantine-Mohamed Boudiaf','Constantine-Mohamed Boudiaf Airport'),(24,3,'Aéroport Batna-Mostafa Ben Boulaid','Batna-Mostafa Ben Boulaid Airport'),(25,3,'Aéroport Jijel-Ferhat Abass','Jijel-Ferhat Abass Airport'),(26,3,'Aéroport Setif','Setif Airport'),(27,3,'Aéroport Biskra-Mohamed Khider','Biskra-Mohamed Khider Airport'),(28,3,'Aéroport Bou saâda','Bou saâda Airport'),(29,3,'Aéroport Laghouat','Laghouat Airport'),(30,3,'Aéroport El Oued-Guemar','El Oued-Guemar Airport'),(31,3,'Aéroport Oum El Bouaghi','Oum El Bouaghi Airport'),(32,3,'Aéroport Touggourt-Sidi Mahdi','Touggourt-Sidi Mahdi Airport'),(33,3,'Aéroport Hassi R\'mel','Hassi R\'mel Airport'),(34,3,'Aéroport Ouargla','Ouargla Airport'),(35,3,'Aéroport Illizi','Illizi Airport'),(36,3,'Aéroport In Salah','In Salah Airport'),(37,3,'Aéroport El Golea','El Golea Airport'),(38,3,'Aéroport In Guezzam','In Guezzam Airport'),(39,3,'Aéroport Adrar-Touat Cheikh Sidi Mohamed Belkabir','Adrar-Touat Cheikh Sidi Mohamed Belkabir Airport'),(40,3,'Aéroport Béchar-Boudghene Ben Ali Lotfi','Béchar-Boudghene Ben Ali Lotfi Airport'),(41,3,'Aéroport El Bayadh','El Bayadh Airport'),(42,3,'Aéroport Mascara-Ghriss','Mascara-Ghriss Airport'),(43,3,'Aéroport Mecheria','Mecheria Airport'),(44,3,'Aéroport Tébessa-Cheikh Larbi Tébessi','Tébessa-Cheikh Larbi Tébessi Airport'),(45,3,'Aéroport Tiaret-Abdelhafid Boussouf Bou Chekif','Tiaret-Abdelhafid Boussouf Bou Chekif Airport'),(46,3,'Aéroport Timimoun','Timimoun Airport'),(47,3,'Aéroport Tindouf','Tindouf Airport'),(48,4,'Aéroport Berlin-Brandenburg','Berlin Brandenburg Airport'),(49,4,'Aéroport Berlin-Tegel','Berlin Tegel Airport'),(50,4,'Aéroport Brême','Bremen Airport'),(51,4,'Aéroport Cologne-Bonn','Cologne Bonn Airport'),(52,4,'Aéroport Dresde','Dresden Airport'),(53,4,'Aéroport Düsseldorf','Düsseldorf Airport'),(54,4,'Aéroport Francfort','Frankfurt Airport'),(55,4,'Aéroport Hambourg','Hamburg Airport'),(56,4,'Aéroport Hanovre','Hannover Airport'),(57,4,'Aéroport Munich','Munich Airport'),(58,4,'Aéroport Munster-Osnabruck','Münster Osnabrück Airport'),(59,4,'Aéroport Nuremberg','Nuremberg Airport'),(60,4,'Aéroport Paderborn','Paderborn Airport'),(61,4,'Aéroport Stuttgart','Stuttgart Airport'),(62,4,'Aéroport Sarrebruck','Saarbrücken Airport'),(63,4,'Aéroport Leipzig-Halle','Leipzig/Halle Airport'),(64,4,'Aéroport Erfurt-Weimar','Erfurt–Weimar Airport'),(65,5,'Aéroport de Cabinda','Cabinda airport'),(66,5,'Aéroport international de Quatro de Fevereiro','Quatro de Fevereiro international airport'),(67,5,'Aéroport de Catoca','Catoca airport'),(68,5,'Aéroport de Kuito','Kuito airport'),(69,6,'Aéroport international du roi Fahd','King Fahd International Airport'),(70,6,'Aéroport international Roi-Abdelaziz','King Abdulaziz International Airport'),(71,6,'Aéroport international du roi Khaled','King Khalid International Airport'),(72,6,'Aéroport international Prince Mohammad Bin Abdulaziz','Prince Mohammad bin Abdulaziz International Airport'),(73,6,'Aéroport d\'Al-Ahsa','Al-Ahsa International Airport'),(74,6,'Aéroport de Yanbu','Yanbu Airport'),(75,7,'Aéroport international d\'Ezeiza','Ezeiza International Airport'),(76,7,'Aéroport international Martín Miguel de Güemes','Martín Miguel de Güemes International Airport'),(77,7,'Aéroport international Piloto Civil Norberto Fernández','Piloto Civil Norberto Fernández International Airport'),(78,8,'Aéroport international de Canberra','Canberra International Airport'),(79,8,'Aéroport international de Sydney-Kingsford Smith','Sydney-Kingsford Smith International Airport'),(80,8,'Aéroport international de Darwin','Darwin International Airport'),(81,8,'Aéroport d\'Alice Springs','Alice Springs Airport'),(82,8,'Aéroport international d\'Adélaïde','Adelaide International Airport'),(83,8,'Aéroport international de Brisbane','Brisbane International Airport'),(84,8,'Aéroport international de Melbourne','Melbourne International Airport'),(85,8,'Aéroport international de Perth','Perth International Airport'),(86,9,'Aéroport international de Bahreïn','Bahrain International Airport'),(87,10,'Aéroport Bruxelles','Brussels Airport'),(88,10,'Aéroport Liège','Liège Airport'),(89,10,'Aéroport Anvers','Antwerp Airport'),(90,11,'Aéroport international de Cotonou','Cotonou international airport'),(91,12,'Aéroport international de Gaborone','Gaborone International Airport'),(92,13,'Aéroport international de São Paulo-Guarulhos','São Paulo–Guarulhos International Airport'),(93,13,'Aéroport international de São Paulo-Congonhas','São Paulo–Congonhas International Airport'),(94,13,'Aéroport international de Rio de Janeiro-Galeão','Rio de Janeiro–Galeão International Airport'),(95,13,'Aéroport international de Brasília-Presidente Juscelino Kubitschek','Brasília–Presidente Juscelino Kubitschek International Airport'),(96,13,'Aéroport international de Belo Horizonte-Confins','Belo Horizonte-Confins International Airport'),(97,13,'Aéroport international de Viracopos','Viracopos International Airport'),(98,13,'Aéroport Santos Dumont','Santos Dumont Airport'),(99,13,'aéroport international de Salvador - Deputado Luís Eduardo Magalhães','Salvador-Deputado Luís Eduardo Magalhães International Airport'),(100,13,'Aéroport international Salgado Filho','Salgado Filho International Airport'),(101,13,'Aéroport international de Recife/Guararapes - Gilberto Freyre','Recife-Guararapes–Gilberto Freyre International Airport'),(102,14,'Aéroport international de Ouagadougou','Ouagadougou international airport'),(103,14,'Aéroport de Bobo Dioulasso','Bobo Dioulasso airport'),(104,15,'Aéroport international de Bujumbura','Bujumbura International Airport'),(105,16,'Aéroport international de Douala','Douala international airport'),(106,16,'Aéroport international de Garoua','Garoua international airport'),(107,16,'Aéroport international de Maroua Salak','Maroua Salak international airport'),(108,16,'Aéroport international de Yaoundé-Nsimalen','Yaoundé-Nsimalen international airport'),(109,17,'Aéroport international de Calgary','Calgary International Airport'),(110,17,'Aéroport international d\'Edmonton','Edmonton International Airport'),(111,17,'Aéroport international Stanfield d\'Halifax','Halifax Stanfield International Airport'),(112,17,'Aéroport international du Grand Moncton','Greater Moncton International Airport'),(113,17,'Aéroport international Pierre-Elliott-Trudeau de Montréal','Montréal–Pierre Elliott Trudeau International Airport'),(114,17,'Aéroport international Macdonald-Cartier d\'Ottawa','Ottawa Macdonald–Cartier International Airport'),(115,17,'Aéroport international Jean-Lesage de Québec','Québec City Jean Lesage International Airport'),(116,17,'Aéroport international de Regina','Regina International Airport'),(117,17,'Aéroport John G. Diefenbaker','Saskatoon John G. Diefenbaker International Airport'),(118,17,'Aéroport international de Saint-Jean de Terre-Neuve','St. John\'s International Airport'),(119,17,'Aéroport de Toronto-City Centre','Toronto-City Center Airport'),(120,17,'Aéroport international Pearson de Toronto','Toronto Pearson International Airport'),(121,17,'Aéroport international de Vancouver','Vancouver International Airport'),(122,17,'Aéroport international James Armstrong Richardson de Winnipeg','Winnipeg James Armstrong Richardson International Airport'),(123,18,'Aéroport international Amílcar-Cabral','Amílcar-Cabral international airport'),(124,18,'Aéroport international de Praia','Praia international airport'),(125,18,'Aéroport international Francisco Mendes','Francisco Mendes international airport'),(126,18,'Aéroport international Cesária-Évora','Cesária-Évora international airport'),(127,19,'Aéroport international Arturo-Merino-Benítez','Comodoro Arturo Merino Benítez International Airport'),(128,20,'Aéroport international de Pékin','Beijing International Airport'),(129,20,'Aéroport de Hong Kong','Hong Kong International Airport'),(130,20,'Aéroport international de Shanghai-Pudong','Shanghai Pudong International Airport'),(131,20,'Aéroport international de Shanghai Hongqiao','Shanghai Hongqiao International Airport'),(132,20,'Aéroport international de Canton Baiyun','Guangzhou Baiyun International Airport'),(133,20,'Aéroport international de Shenzhen Bao\'an','Shenzhen Bao\'an International Airport'),(134,20,'Aéroport international de Zhengzhou Xinzheng','Zhengzhou Xinzheng International Airport'),(135,20,'Aéroport international de Kunming Changshui','Kunming Changshui International Airport'),(136,21,'Aéroport international El Dorado','El Dorado International Airport'),(137,21,'Aéroport international José María Córdova','José María Córdova International Airport'),(138,21,'Aéroport international Alfonso Bonilla Aragón','Alfonso Bonilla Aragón International Airport'),(139,22,'Aéroport international d\'Incheon','Incheon International Airport'),(140,22,'Aéroport international de Jeju','Jeju International Airport'),(141,22,'Aéroport international de Gimpo','Gimpo International Airport'),(142,22,'Aéroport international de Gimhae','Gimhae International Airport'),(143,23,'Aéroport international Félix-Houphouët-Boigny','Félix-Houphouët-Boigny international airport'),(144,23,'Aéroport international de Yamoussoukro','Yamoussoukro international airport'),(145,24,'Aéroport international Ambouli','Ambouli International Airport'),(146,25,'Aéroport international Le Caire','Cairo International Airport'),(147,25,'Aéroport Abou Simbel','Abu Simbel Airport'),(148,25,'Aéroport international Marsa Alam','Marsa Alam International Airport'),(149,25,'Aéroport international El-Arish','El Arish International Airport'),(150,25,'Aéroport international Charm el-Cheikh','Sharm el-Sheikh International Airport'),(151,25,'Aéroport international Hurghada','Hurghada International Airport'),(152,25,'Aéroport international Taba','Taba International Airport'),(153,26,'Aéroport international d\'Abou Dabi','Abu Dhabi International Airport'),(154,26,'Aéroport international d\'Al Ain','Al Ain International Airport'),(155,26,'Aéroport international Al Maktoum','Al Maktoum International Airport'),(156,26,'Aéroport international de Charjah','Sharjah International Airport'),(157,26,'Aéroport international de Dubaï','Dubai International Airport'),(158,26,'Aéroport international de Fujaïrah','Fujairah International Airport'),(159,26,'Aéroport international de Ras Al Khaimah','Ras Al Khaimah International Airport'),(160,27,'Aéroport international Mariscal Sucre','Mariscal Sucre International Airport'),(161,28,'Aéroport international d\'Asmara','Asmara International Airport'),(162,29,'Aéroport Alicante','Alicante–Elche Airport'),(163,29,'Aéroport Barcelone','Barcelona–El Prat Airport'),(164,29,'Aéroport Bilbao','Bilbao Airport'),(165,29,'Aéroport La Corogne','A Coruña Airport'),(166,29,'Aéroport Ibiza','Ibiza Airport'),(167,29,'Aéroport Lanzarote','Lanzarote Airport'),(168,29,'Aéroport Madrid-Barajas','Madrid-Barajas Airport'),(169,29,'Aéroport Malaga','Málaga Airport'),(170,29,'Aéroport Palma de Majorque','Palma de Mallorca Airport'),(171,29,'Aéroport Seville','Seville Airport'),(172,29,'Aéroport Tenerife','Tenerife Airport'),(173,29,'Aéroport Valence','Valencia Airport'),(174,30,'Aéroport international John-F.-Kennedy, NY','John-F.-Kenned international airport , NY'),(175,30,'Aéroport international Newark Liberty, NY','Newark Liberty international airport , NY'),(176,30,'Aéroport international de Los Angeles, CA','Los Angeles international airport , CA'),(177,30,'Aéroport international de San Diego, CA','San Diego international airport , CA'),(178,30,'Aéroport international de San Francisco, CA','San Francisco international airport , CA'),(179,30,'Aéroport international de Dallas-Fort Worth, TX','Dallas-Fort Worth international airport, TX'),(180,30,'Aéroport intercontinental George-Bush de Houston, TX','Houston George-Bush intercontinental airport , TX'),(181,30,'Aéroport international de San Antonio, TX','San Antonio international airport , TX'),(182,30,'Aéroport international de Miami, FL','Miami international airport , FL'),(183,30,'Aéroport international d\'Orlando, FL','Orlando international airport, FL'),(184,30,'Aéroport international O\'Hare de Chicago, IL','Chicago O\'Hare international airport , IL'),(185,30,'Aéroport international Midway de Chicago, IL','Chicago Midway international airport , IL'),(186,30,'Aéroport international de Gary/Chicago, IL','Gary/Chicago international airport , IL'),(187,30,'Aéroport international de Washington-Dulles','Washington-Dulles international airport'),(188,30,'Aéroport national Ronald Reagan','Ronald Reagan national airport'),(189,31,'Aéroport international de Bole Addis Ababa','Addis Ababa Bole International Airport'),(190,31,'Aéroport international d\'Aba Tenna Dejazmach Yilma','Aba Tenna Dejazmach Yilma International Airport'),(191,31,'Aéroport de Arba Minch','Arba Minch Airport'),(192,31,'Aéroport de Asosa','Asosa Airport'),(193,31,'Aéroport de Axum','Axum Airport'),(194,31,'Aéroport de Bahir Dar','Bahir Dar Airport'),(195,31,'Aéroport de Combolcha','Combolcha Airport'),(196,31,'Aéroport de Gambela','Gambela Airport'),(197,31,'Aéroport de Robe','Robe Airport'),(198,31,'Aéroport de Gode','Gode Airport'),(199,31,'Aéroport de Gondar','Gondar Airport'),(200,31,'Aéroport de Jijiga','Jijiga Airport'),(201,31,'Aéroport de Aba Segud','Aba Segud Airport'),(202,31,'Aéroport de Kabri Dar','Kabri Dar Airport'),(203,31,'Aéroport de Lalibela','Lalibela Airport'),(204,31,'Aéroport de Alula Aba Nega','Alula Aba Nega Airport'),(205,32,'Aéroport Paris-Charles de Gaulle','Paris-Charles de Gaulle Airport'),(206,32,'Aéroport Paris-Orly','Paris-Orly Airport'),(207,32,'Aéroport Marseille-Provence','Marseille-Provence Airport'),(208,32,'Aéroport Nice-Côte d\'Azur','Nice-Côte d\'Azur Airport'),(209,32,'Aéroport Toulouse-Blagnac','Toulouse-Blagnac Airport'),(210,32,'Aéroport Montpellier-Méditerranée','Montpellier-Méditerranée Airport'),(211,32,'Aéroport Lyon-Saint-Exupéry','Lyon-Saint-Exupéry Airport'),(212,32,'Aéroport Nantes-Atlantique','Nantes-Atlantique Airport'),(213,32,'Aéroport Lille-Lesquin','Lille-Lesquin Airport'),(214,32,'Aéroport Strasbourg-Entzheim','Strasbourg-Entzheim Airport'),(215,32,'Aéroport Toulon/Hyères-Le Palyvestre','Toulon/Hyères-Le Palyvestre Airport'),(216,32,'Aéroport Bordeaux-Mérignac','Bordeaux-Mérignac Airport'),(217,32,'Aéroport Bastia-Poretta','Bastia-Poretta Airport'),(218,32,'Aéroport Caen-Carpiquet','Caen-Carpiquet Airport'),(219,32,'Aéroport Ajaccio-Campo dell\' Oro','Ajaccio-Campo dell\' Oro Airport'),(220,32,'Aéroport Rennes-Saint-Jacques','Rennes-Saint-Jacques Airport'),(221,32,'Aéroport St Etienne-Bouthéon','St Etienne-Bouthéon Airport'),(222,34,'Aéroport international Léon-Mba','Léon-Mba international airport'),(223,34,'Aéroport international de Port-Gentil','Port-Gentil international airport'),(224,35,'Aéroport international de Kotoka','Kotoka international airport'),(225,35,'Aéroport de Kumasi','kumasi airport'),(226,36,'Aéroport international de Conakry','Conakry international airport'),(227,37,'Aéroport international Osvaldo Vieira de Bissau','Osvaldo Vieira international airport'),(228,38,'Aéroport international de Malabo','Malabo international airport'),(229,38,'Aéroport international Obiang Nguema','Obiang Nguema international airport'),(230,39,'Indira Gandhi International Airport','Aéroport international Indira-Gandhi'),(231,39,'Aéroport international de Goa','Goa International Airport'),(232,39,'Aéroport international d\'Hyderabad','Hyderabad International Airport'),(233,39,'Aéroport international de Mangalore','Mangalore International Airport'),(234,39,'Aéroport international Chhatrapati-Shivaji','Chhatrapati Shivaji International Airport'),(235,39,'Aéroport international de Trivandrum','Trivandrum International Airport'),(236,39,'Aéroport international de Chennai','Chennai International Airport'),(237,39,'Aéroport international de Calicut','Calicut International Airport'),(238,39,'Aéroport international de Jaipur','Jaipur Airport'),(239,39,'Aéroport international Netaji-Subhash-Chandra-Bose','Netaji Subhas Chandra Bose International Airport'),(240,40,'Aéroport international Husein Sastranegara','Husein Sastranegara International Airport'),(241,40,'Aéroport Halim-Perdanakusuma','Halim Perdanakusuma Airport'),(242,40,'Aéroport international Soekarno-Hatta','Soekarno–Hatta International Airport'),(243,40,'Aéroport international Achmad Yani','Achmad Yani International Airport'),(244,40,'Aéroport international Juanda','Juanda International Airport'),(245,40,'Aéroport international Ngurah Rai','Ngurah Rai International Airport'),(246,40,'Aéroport international Sultan Hasanuddin','Sultan Hasanuddin International Airport'),(247,40,'Aéroport international de Kualanamu','Kualanamu International Airport'),(248,40,'Aéroport international Sultan Aji Muhamad Sulaiman','Sultan Aji Muhammad Sulaiman Airport'),(249,40,'Aéroport international Adisutjipto','Adisucipto International Airport'),(250,40,'Aéroport Syamsudin Noor','Syamsudin Noor International Airport'),(251,40,'Aéroport international Sultan Mahmud Badaruddin II','Sultan Mahmud Badaruddin II International Airport'),(252,40,'Aéroport international Sultan Syarif Qasim II','Sultan Syarif Kasim II International Airport'),(253,40,'Aéroport international Minangkabau','Minangkabau International Airport'),(254,40,'Aéroport international Sam Ratulangi','Sam Ratulangi International Airport'),(255,41,'Aéroport international d\'Al Najaf','Al Najaf International Airport'),(256,41,'Aéroport international de Bagdad','Baghdad International Airport'),(257,41,'Aéroport international de Bassorah','Basra International Airport'),(258,41,'Aéroport international d\'Erbil','Erbil International Airport'),(259,41,'Aéroport international de Mosul','Mosul International Airport'),(260,41,'Aéroport international de Sulaimaniyah','Sulaimaniyah International Airport'),(261,42,'Aéroport international Arak','Arak International Airport'),(262,42,'Aéroport international de Bandar Abbas','Bandar Abbas International Airport'),(263,42,'Aéroport international de Birjand','Birjand International Airport'),(264,42,'Aéroport international d\'Ispahan','Isfahan International Airport'),(265,42,'Aéroport de Kermanshah','Kermanshah Airport'),(266,42,'Aéroport international de Kish','Kish International Airport'),(267,42,'Aéroport international de Lar','Larestan International Airport'),(268,42,'Aéroport international de Mashhad','Mashhad International Airport'),(269,42,'Aéroport international de Qom','Qom International Airport'),(270,42,'Aéroport international de Shiraz','Shiraz International Airport'),(271,42,'Aéroport international de Tabriz','Tabriz International Airport'),(272,42,'Aéroport international Imam Khomeini-Téhéran','Tehran Imam Khomeini International Airport'),(273,42,'Aéroport international Mehrabad','Mehrabad International Airport'),(274,43,'Aéroport Alghero-Fertilia','Alghero-Fertilia Airport'),(275,43,'Aéroport Ancône-Falconara','Ancona Falconara Airport'),(276,43,'Aéroport Bari-Palese','Bari Palese Airport'),(277,43,'Aéroport Bologne-Guglielmo Marconi','Bologna Guglielmo Marconi Airport'),(278,43,'Aéroport Brindisi-Casale','Brindisi – Salento Airport'),(279,43,'Aéroport Cagliari-Elmas','Cagliari Elmas Airport'),(280,43,'Aéroport Catane-Fontanarossa','Catania–Fontanarossa Airport'),(281,43,'Aéroport Florence-Peretola','Florence Airport-Peretola'),(282,43,'Aéroport Gênes-Cristoforo Colombo','Genoa Cristoforo Colombo Airport'),(283,43,'Aéroport L\'Aquila','L\'Aquila–Preturo Airport'),(284,43,'Aéroport Lamezia-Terme','Lamezia Terme Airport'),(285,43,'Aéroport Lampedusa','Lampedusa Airport'),(286,43,'Aéroport Milan Linate','Milan Linate Airport'),(287,43,'Aéroport Milan Malpensa','Milan–Malpensa Airport'),(288,43,'Aéroport Naples','Naples Airport'),(289,43,'Aéroport Palerme','Palermo Airport'),(290,43,'Aéroport Pantelleria','Pantelleria Airport'),(291,43,'Aéroport Pescara-Abruzzo','Abruzzo Airport'),(292,43,'Aéroport Pise-Galileo Galilei','Pisa Airport'),(293,43,'Aéroport Reggio de Calabre','Reggio Calabria Airport'),(294,43,'Aéroport Federico Fellini','Federico Fellini Airport'),(295,43,'Aéroport Rome-Fiumicino','Rome Fiumicino Airport'),(296,43,'Aéroport Rome-Ciampino','Rome Ciampino Airport'),(297,43,'Aéroport Trapani-Birgi','Vincenzo Florio Airport Trapani'),(298,43,'Aéroport Trieste-Ronchi dei Legionari','Ronchi dei Legionari airport'),(299,43,'Aéroport Turin-Caselle','Turin-Caselle Airport'),(300,43,'Aéroport Venise-Marco Polo','Venice Marco Polo Airport'),(301,43,'Aéroport Verone-Villafranca','Verona Villafranca Airport'),(302,46,'Aéroport international de Narita','Narita International Airport'),(303,46,'Aéroport international de Tokyo-Haneda','Haneda International Airport'),(304,46,'Aéroport international d\'Osaka','Osaka International Airport'),(305,47,'Aéroport international Reine-Alia','Queen Alia International Airport'),(306,47,'Aéroport civil d\'Amman','Amman Civil Airport'),(307,47,'Aéroport international du Rois Hussein','King Hussein International Airport'),(308,48,'Aéroport international d\'Aktaou','Aktau International Airport'),(309,48,'Aéroport international d\'Aktioubé','Aktobe International Airport'),(310,48,'Aéroport international d\'Almaty','Almaty International Airport'),(311,48,'Aéroport international d\'Astana','Astana International Airport'),(312,48,'Aéroport international d\'Atyraou','Atyrau International Airport'),(313,48,'Aéroport international de Chimkent','Shymkent International Airport'),(314,49,'Aéroport international Jomo-Kenyatta','Jomo Kenyatta International Airport'),(315,49,'Aéroport international d\'Eldoret','Eldoret International Airport'),(316,49,'Aéroport international de Mombasa','Mombasa International Airport'),(317,50,'Aéroport international de Koweït','Kuwait International Airport'),(318,51,'Aéroport international de Beyrouth - Rafic Hariri','Beirut–Rafic Hariri International Airport'),(319,52,'Aéroport international de Monrovia-Roberts','Monrovia-Roberts international airport'),(320,53,'Aéroport international de Benina','Benina international airport'),(321,53,'Aéroport international de Tripoli','Tripoli international airport'),(322,54,'Aéroport international de Fascene','Fascene International Airport'),(323,54,'Aéroport international d\'Ivato','Ivato International Airport'),(324,55,'Aéroport international de Kuala Lumpur','Kuala Lumpur International Airport'),(325,55,'Aéroport international de Kota Kinabalu','Kota Kinabalu International Airport'),(326,55,'Aéroport international de Penang','Penang International Airport'),(327,55,'Aéroport international de Kuching','Kuching International Airport'),(328,55,'Aéroport de Miri','Miri Airport'),(329,56,'Aéroport international de Chileka','Chileka International Airport'),(330,56,'Aéroport international de Lilongwe','Lilongwe International Airport'),(331,57,'Aéroport de Kidal','kidal airport'),(332,57,'Aéroport international de Bamako-Sénou','Bamako Sénou international airport'),(333,57,'Aéroport international de Gao Korogoussou','Bamako Korogoussou international airport'),(334,57,'Aéroport international de Kayes Dag Dag','Kayes Dag Dag international airport'),(335,57,'Aéroport international de Mopti Ambodédjo','Mopti Ambodédjo international airport'),(336,57,'Aéroport international de Sikasso Dignagan','Sikasso Dignagan international airport'),(337,57,'Aéroport international de Tombouctou','Tombouctou international airport'),(338,58,'Aéroport Casablanca-Mohammed V','Casablanca-Mohammed V Airport'),(339,58,'Aéroport Marrakech -Menara','Marrakech -Menara Airport'),(340,58,'Aéroport Agadir-Al Massira','Agadir-Al Massira Airport'),(341,58,'Aéroport Tanger-Ibn Batouta','Tanger-Ibn Batouta Airport'),(342,58,'Aéroport Fès-Saïss','Fès-Saïss Airport'),(343,58,'Aéroport Oujda-Angad','Oujda-Angad Airport'),(344,58,'Aéroport Nador-Al Aroui','Nador-Al Aroui Airport'),(345,58,'Aéroport Rabat-Salé','Rabat-Salé Airport'),(346,58,'Aéroport Essaouira','Essaouira Airport'),(347,58,'Aéroport Ouarzazate','Ouarzazate Airport'),(348,59,'Aéroport international d\'Atar','Atar international airport'),(349,59,'Aéroport international de Nouakchott','Nouakchott international airport'),(350,60,'Aéroport international de Mexico','Mexico City International Airport'),(351,60,'Aéroport international de Cancún','Cancún International Airport'),(352,60,'Aéroport international de Guadalajara','Guadalajara International Airport'),(353,60,'Aéroport international de Monterrey','Monterrey International Airport'),(354,60,'Aéroport international de Tijuana','Tijuana International Airport'),(355,61,'Aéroport international de Maputo','Maputo International Airport'),(356,61,'Aéroport de Bazaruto Island','Bazaruto Island Airport'),(357,61,'Aéroport de Beira','Beira Airport'),(358,61,'Aéroport de Inhambane','Inhambane Airport'),(359,61,'Aéroport de Nampula','Nampula Airport'),(360,61,'Aéroport de Pemba','Pemba Airport'),(361,61,'Aéroport de Quelimane','Quelimane Airport'),(362,61,'Aéroport de Chingozi','Chingozi Airport'),(363,61,'Aéroport de Vilankulo','Vilankulo Airport'),(364,62,'Aéroport international Hosea Kutako','Hosea Kutako international airport'),(365,63,'Aéroport international de Zinder','Zinder international airport'),(366,63,'Aéroport international Diori Hamani','Diori Hamani international airport'),(367,63,'Aéroport international Mano Dayak','Mano Dayak international airport'),(368,64,'Aéroport international Nnamdi Azikiwe','Nnamdi Azikiwe international airport'),(369,64,'Aéroport international Murtala-Muhammed','Murtala-Muhammed international airport'),(370,64,'Aéroport international de Port Harcourt','Port Harcourt international airport'),(371,65,'Aéroport international de Mascate','Muscat International Airport'),(372,66,'Aéroport international d\'Entebbe','Entebbe International Airport'),(373,67,'Aéroport international de Tachkent','Tashkent International Airport'),(374,67,'Aéroport international de Bukhara','Bukhara International Airport'),(375,67,'Aéroport international de Fergana','Fergana International Airport'),(376,67,'Aéroport international de Navoi','Navoi International Airport'),(377,67,'Aéroport international de Samarcande','Samarkand International Airport'),(378,68,'Aéroport international Benazir Bhutto','Benazir Bhutto International Airport'),(379,68,'Aéroport international Jinnah','Jinnah International Airport'),(380,68,'Aéroport international de Peshawar','Peshawar International Airport'),(381,68,'Aéroport international de Lahor','Lahore International Airport'),(382,69,'Aéroport Amsterdam-Schiphol','Amsterdam Airport Schiphol'),(383,69,'Aéroport Eindhoven','Eindhoven Airport'),(384,69,'Aéroport Rotterdam','Rotterdam The Hague Airport'),(385,70,'Aéroport international Jorge-Chávez','Jorge Chávez International Airport'),(386,70,'Aéroport international Alejandro Velasco Astete','Alejandro Velasco Astete International Airport'),(387,71,'Aéroport international Ninoy Aquino','Ninoy Aquino International Airport'),(388,71,'Aéroport international de Clark','Clark International Airport'),(389,71,'Aéroport international de Mactan-Cebu','Mactan–Cebu International Airport'),(390,72,'Aéroport international de Doha','Doha International Airport'),(391,72,'Aéroport international Hamad','Hamad International Airport'),(392,73,'Aéroport international de Bangui','Bangui international airport'),(393,74,'Aéroport international de Goma','Goma international airport'),(394,74,'Aéroport international de Lubumbashi','Lubumbashi international airport'),(395,74,'Aéroport international de Ndjili','Ndjili international airport'),(396,75,'Aéroport international Maya-Maya','Maya-Maya international airport'),(397,75,'Aéroport international Agostinho-Neto','Agostinho-Neto international airport'),(398,76,'Aéroport Aberdeen-Dyce','Aberdeen Airport'),(399,76,'Aéroport Belfast-George Best','George Best Belfast City Airport'),(400,76,'Aéroport Belfast-aéroport International','Belfast International Airport'),(401,76,'Aéroport Birmingham','Birmingham Airport'),(402,76,'Aéroport Bristol','Bristol Airport'),(403,76,'Aéroport Cambridge','Cambridge Airport'),(404,76,'Aéroport Cardiff','Cardiff Airport'),(405,76,'Aéroport Dundee','Dundee Airport'),(406,76,'Aéroport Durham-Tees Valley','Durham-Tees Valley Airport'),(407,76,'Aéroport Edimbourg','Edimbourg Airport'),(408,76,'Aéroport Exeter','Exeter Airport'),(409,76,'Aéroport Glasgow','Glasgow Airport'),(410,76,'Aéroport Guernesey','Guernesey Airport'),(411,76,'Aéroport Humberside','Humberside Airport'),(412,76,'Aéroport Île de Man-Ronaldsway','Man-Ronaldsway Airport'),(413,76,'Aéroport Inverness','Inverness Airport'),(414,76,'Aéroport Jersey','Jersey Airport'),(415,76,'Aéroport Leeds-Bradford','Leeds-Bradford Airport'),(416,76,'Aéroport Liverpool','Liverpool Airport'),(417,76,'Aéroport Londres-Gatwick','London-Gatwick Airport'),(418,76,'Aéroport Londres-Heathrow','London-Heathrow Airport'),(419,76,'Aéroport Londres-Luton','London-Luton Airport'),(420,76,'Aéroport Manchester','Manchester Airport'),(421,76,'Aéroport Newcastle','Newcastle Airport'),(422,76,'Aéroport Norwich','Norwich Airport'),(423,76,'Aéroport Nottingham','Nottingham Airport'),(424,76,'Aéroport Southampton','Southampton Airport'),(425,77,'Aéroport de Moscou-Cheremetievo','Sheremetyevo International Airport'),(426,77,'Aéroport de Moscou-Domodedovo','Domodedovo International Airport'),(427,77,'Aéroport international de Vnoukovo','Vnukovo International Airport'),(428,77,'Aéroport international de Vladivostok','Vladivostok International Airport'),(429,77,'Aéroport international de Sotchi','Sochi International Airport'),(430,77,'Aéroport international de Volgograd','Volgograd International Airport'),(431,77,'Aéroport international de Sébastopol','Sebastopol International Airport'),(432,77,'Aéroport international de Kazan','Kazan International Airport'),(433,77,'Aéroport international Pulkovo','Pulkovo International Airport'),(434,78,'Aéroport de Butare','Butare Airport'),(435,78,'Aéroport de Kigali','Kigali International Airport'),(436,79,'Aéroport de Cap Skirring','Cap skirring airport'),(437,79,'Aéroport international Léopold-Sédar-Senghor','Leopold Sedar Senghor international airport'),(438,79,'Aérodrome de Kaolack','Kaolack aerodrome'),(439,79,'Aéroport de Saint-Louis','Saint Louis airport'),(440,79,'Aéroport de Ziguinchor','Ziguinchor airport'),(441,80,'Aéroport international de Lungi','Lungi international airport'),(442,80,'Aéroport international de Sherbro','Sherbro international airport'),(443,81,'Aéroport international de Singapour Changi','Singapore Changi International Airport'),(444,81,'Aéroport Seletar','Seletar Airport'),(445,82,'Aéroport international de Bender Qassim','Bender Qassim International Airport'),(446,82,'Aéroport international d\'Abdullahi Yusuf','Abdullahi Yusuf International Airport'),(447,82,'Aéroport international de Hargeisa','Hargeisa International Airport'),(448,82,'Aéroport international de Aden Adde','Aden Adde International Airport'),(449,83,'Aéroport international de Khartoum','Khartoum International Airport'),(450,83,'Aéroport de Atbara','Atbara Airport'),(451,83,'Aéroport de Dongola','Dongola Airport'),(452,83,'Aéroport de El Fasher','El Fasher Airport'),(453,83,'Aéroport de El Obeid','El Obeid Airport'),(454,83,'Aéroport de Kassala','Kassala Airport'),(455,83,'Aéroport de Nyala','Nyala Airport'),(456,83,'Aéroport international de Port Soudan','Port Sudan New International Airport'),(457,83,'Aéroport de Wadi Halfa','Wadi Halfa Airport'),(458,84,'Aéroport international d\'Alep','Aleppo International Airport'),(459,84,'Aéroport international de Damas','Damascus International Airport'),(460,84,'Aéroport international de Bassel El Assad','Bassel Al-Assad International Airport'),(461,85,'Aéroport international de Douchanbé','Dushanbe International Airport'),(462,85,'Aéroport international de Qurghonteppa','Qurghonteppa International Airport'),(463,86,'Aéroport international Taiwan Taoyuan','Taoyuan International Airport'),(464,86,'Aéroport de Taipei Songshan','Taipei Songshan Airport'),(465,87,'Aéroport international Julius Nyerere','Julius Nyerere International Airport'),(466,87,'Aéroport international du Kilimandjaro','Kilimanjaro International Airport'),(467,88,'Aéroport international de Ndjamena','Ndjamena international airport'),(468,89,'Aéroport Suvarnabhumi de Bangkok','Suvarnabhumi Airport'),(469,89,'Aéroport international Don Muang','Don Mueang International Airport'),(470,89,'Aéroport international de Hat Yai','Hat Yai International Airport'),(471,90,'Aéroport international de Lomé-Tokoin','Lomé-Tokoin international airport'),(472,91,'Aéroport Djerba-Zarzis','Djerba-Zarzis Airport'),(473,91,'Aéroport El Borma','El Borma Airport'),(474,91,'Aéroport Enfidha-Hammamet','Enfidha-Hammamet Airport'),(475,91,'Aéroport Gabès-Matmata','Gabès-Matmata Airport'),(476,91,'Aéroport Gafsa-Ksar','Gafsa-Ksar Airport'),(477,91,'Aéroport Monastir Habib-Bourguiba','Monastir Habib-Bourguiba Airport'),(478,91,'Aéroport Sfax-Thyna','Sfax-Thyna Airport'),(479,91,'Aéroport Tabarka-Aïn Draham','Tabarka-Aïn Draham Airport'),(480,91,'Aéroport Tozeur-Nefta','Tozeur-Nefta Airport'),(481,91,'Aéroport Tunis-Carthage','Tunis-Carthage Airport'),(482,92,'Aéroport d\'Achgabat','Ashgabat International Airport'),(483,93,'Aéroport de Adana Şakirpaşa','Adana Şakirpaşa Airport'),(484,93,'Aéroport international de Esenboğa','Esenboğa International Airport'),(485,93,'Aéroport de Hatay','Hatay Airport'),(486,93,'Aéroport de Antalya','Antalya Airport'),(487,93,'Aéroport de Milas-Bodrum','Milas-Bodrum Airport'),(488,93,'Aéroport de Yenişehir','Yenişehir Airport'),(489,93,'Aéroport de Dalaman','Dalaman Airport'),(490,93,'Aéroport de Erzurum','Erzurum Airport'),(491,93,'Aéroport de Anadolu','Anadolu Airport'),(492,93,'Aéroport de Gaziantep Oğuzeli','Gaziantep Oğuzeli Airport'),(493,93,'Aéroport international de Atatürk','Atatürk International Airport'),(494,93,'Aéroport international de Sabiha Gökçen','Sabiha Gökçen International Airport'),(495,93,'Aéroport international de Adnan Menderes','Adnan Menderes International Airport'),(496,93,'Aéroport international de Erkilet','Erkilet International Airport'),(497,93,'Aéroport de Konya','Konya Airport'),(498,93,'Aéroport de Zafer','Zafer Airport'),(499,93,'Aéroport de Malatya Erhaç','Malatya Erhaç Airport'),(500,93,'Aéroport de Nevşehir Kapadokya','Nevşehir Kapadokya Airport'),(501,93,'Aéroport de Samsun-Çarşamba','Samsun-Çarşamba Airport'),(502,93,'Aéroport de Şanlıurfa GAP','Şanlıurfa GAP Airport'),(503,93,'Aéroport de Trabzon','Trabzon Airport'),(504,94,'Aéroport international de Carrasco','Carrasco International Airport'),(505,95,'Aéroport international Maiquetía - Simón Bolívar','Simón Bolívar International Airport'),(506,96,'Aéroport international de','Tan Son Nhat International Airport'),(507,96,'Aéroport international de','Noi Bai International Airport'),(508,96,'Aéroport international de','Da Nang International Airport'),(509,97,'Aéroport international d\'Aden','Aden International Airport'),(510,97,'Aéroport international de Hodeida','Hodeida International Airport'),(511,97,'Aéroport international de Sana\'a','Sana\'a International Airport'),(512,97,'Aéroport international de Ta\'izz','Ta\'izz International Airport'),(513,98,'Aéroport international de Harry Mwanga Nkumbula','Harry Mwanga Nkumbula International Airport'),(514,98,'Aéroport international de Kenneth Kaunda','Kenneth Kaunda International Airport'),(515,98,'Aéroport de Mfuwe','Mfuwe Airport'),(516,98,'Aéroport international de Simon Mwansa Kapwepwe','Simon Mwansa Kapwepwe International Airport'),(517,99,'Aéroport international de Joshua Mqabuko Nkomo','Joshua Mqabuko Nkomo International Airport'),(518,99,'Aéroport international de Harare','Harare International Airport'),(519,99,'Aéroport de Victoria Falls','Victoria Falls Airport');
/*!40000 ALTER TABLE `airports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carriage`
--

DROP TABLE IF EXISTS `carriage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carriage` (
  `id_carriage` mediumint(9) NOT NULL AUTO_INCREMENT,
  `id_type` tinyint(4) NOT NULL,
  `id_weight` tinyint(4) NOT NULL,
  `price` varchar(50) NOT NULL,
  `id_currency` tinyint(4) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `id_user` mediumint(9) NOT NULL,
  PRIMARY KEY (`id_carriage`),
  KEY `id_info` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carriage`
--

LOCK TABLES `carriage` WRITE;
/*!40000 ALTER TABLE `carriage` DISABLE KEYS */;
INSERT INTO `carriage` VALUES (80,8,6,'50 USD',0,'Cette annonce est un exemple type.\r\nThis announce is set as an example.',42),(81,4,5,'60 CAD',0,'Cette annonce est un exemple type. This announce is set as an example.',42),(82,7,6,'40 EUR',0,'Cette annonce est un exemple type. This announce is set as an example.',42),(83,5,5,'30 CAD',0,'Cette annonce est un exemple type. This announce is set as an example.',42),(84,1,2,'10 CAD',0,'Bonjour,\r\nJe prends des documents non volumineux que je mettrai dans mon bagage à main.\r\n\r\nMerci,',49),(85,1,4,'40 EUR',0,' ',42),(86,1,4,'40 EUR',0,' ',42);
/*!40000 ALTER TABLE `carriage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id_country` tinyint(4) NOT NULL AUTO_INCREMENT,
  `country` varchar(34) DEFAULT NULL,
  `country_tr` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Afghanistan','Afghanistan'),(2,'Afrique du Sud','South Africa'),(3,'Algérie','Algeria'),(4,'Allemagne','Germany'),(5,'Angola','Angola'),(6,'arabie saoudite','Saudi Arabia'),(7,'Argentine','Argentina'),(8,'Australie','Australia'),(9,'Bahreïn','Bahrain'),(10,'Belgique','Belgium'),(11,'Benin','Benin'),(12,'Botswana','Botswana'),(13,'Brésil','Brazil'),(14,'Burkina faso','Burkina Faso'),(15,'Burundi','Burundi'),(16,'Cameroun','Cameroon'),(17,'Canada','Canada'),(18,'Cap-Vert','Cape verde'),(19,'Chili','Chile'),(20,'Chine','China'),(21,'Colombie','Colombia'),(22,'Corée du sud','South korea'),(23,'Côte d\'ivoire','Ivory Coast'),(24,'Djibouti','Djibouti'),(25,'Egypte','Egypt'),(26,'Émirats arabes unis','United Arab Emirates'),(27,'Équateur','Ecuador'),(28,'Érythrée','Djibouti'),(29,'Espagne','Spain'),(30,'Etats-Unis','United States'),(31,'Éthiopie','Ethiopia'),(32,'France','France'),(34,'Gabon','Gabon'),(35,'Ghana','Ghana'),(36,'Guinée','Guinea'),(37,'Guinée Bissau','Guinea-Bissau'),(38,'Guinée Equatoriale','Equatorial Guinea'),(39,'Inde','India'),(40,'Indonésie','Indonesia'),(41,'Irak','Iraq'),(42,'Iran','Iran'),(43,'Italie','Italy'),(46,'Japon','Japan'),(47,'Jordanie','Jordan'),(48,'Kazakhstan','Kazakhstan'),(49,'Kenya','Kenya'),(50,'Koweït','Kuwait'),(51,'Liban','Lebanon'),(52,'Liberia','Liberia'),(53,'Libye','Libya'),(54,'Madagascar','Madagascar'),(55,'Malaisie','Malaysia'),(56,'Malawi','Malawi'),(57,'Mali','Mali'),(58,'Maroc','Morocco'),(59,'Mauritanie','Mauritania'),(60,'Mexique','Mexico'),(61,'Mozambique','Mozambique'),(62,'Namibie','Namibia'),(63,'Niger','Niger'),(64,'Nigéria','Nigeria'),(65,'Oman','Oman'),(66,'Ouganda','Uganda'),(67,'Ouzbékistan','Uzbekistan'),(68,'Pakistan','Pakistan'),(69,'Pays-bas','Netherlands'),(70,'Pérou','Peru'),(71,'Philippines','Philippines'),(72,'Qatar','Qatar'),(73,'République Centrafrcaine','Central African Republic'),(74,'République démocratique du Congo','Democratic Republic of the Congo'),(75,'République du Congo','Republic of the Congo'),(76,'Royaume Uni','United Kingdom'),(77,'Russie','Russia'),(78,'Rwanda','Rwanda'),(79,'Sénégal','Senegal'),(80,'Sierra Leone','Sierra Leone'),(81,'Singapour','Singapore'),(82,'Somalie','Somalia'),(83,'Soudan','Sudan'),(84,'Syrie','Syria'),(85,'Tadjikistan','Tajikistan'),(86,'Taiwan','Taiwan'),(87,'Tanzanie','Tanzania'),(88,'Tchad','Chad'),(89,'Thaïlande','Thailand'),(90,'Togo','Togo'),(91,'Tunisie','Tunisia'),(92,'Turkménistan','Turkmenistan'),(93,'Turquie','Turkey'),(94,'Uruguay','Uruguay'),(95,'Venezuela','Venezuela'),(96,'Viêt Nam','Vietnam'),(97,'Yémen','Yemen'),(98,'Zambie','Zambia'),(99,'Zimbabwe','Zimbabwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `id_currency` smallint(6) NOT NULL AUTO_INCREMENT,
  `iso` varchar(3) NOT NULL,
  `name_currency` varchar(50) NOT NULL,
  PRIMARY KEY (`id_currency`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (1,'KRW','(South) Korean Won'),(2,'AFA','Afghanistan Afghani'),(3,'ALL','Albanian Lek'),(4,'DZD','Algerian Dinar'),(5,'ADP','Andorran Peseta'),(6,'AOK','Angolan Kwanza'),(7,'ARS','Argentine Peso'),(8,'AMD','Armenian Dram'),(9,'AWG','Aruban Florin'),(10,'AUD','Australian Dollar'),(11,'BSD','Bahamian Dollar'),(12,'BHD','Bahraini Dinar'),(13,'BDT','Bangladeshi Taka'),(14,'BBD','Barbados Dollar'),(15,'BZD','Belize Dollar'),(16,'BMD','Bermudian Dollar'),(17,'BTN','Bhutan Ngultrum'),(18,'BOB','Bolivian Boliviano'),(19,'BWP','Botswanian Pula'),(20,'BRL','Brazilian Real'),(21,'GBP','British Pound'),(22,'BND','Brunei Dollar'),(23,'BGN','Bulgarian Lev'),(24,'BUK','Burma Kyat'),(25,'BIF','Burundi Franc'),(26,'CAD','Canadian Dollar'),(27,'CVE','Cape Verde Escudo'),(28,'KYD','Cayman Islands Dollar'),(29,'CLP','Chilean Peso'),(30,'CLF','Chilean Unidades de Fomento'),(31,'COP','Colombian Peso'),(32,'XOF','Communauté Financière Africaine BCEAO - Francs'),(33,'XAF','Communauté Financière Africaine BEAC, Francs'),(34,'KMF','Comoros Franc'),(35,'XPF','Comptoirs Français du Pacifique Francs'),(36,'CRC','Costa Rican Colon'),(37,'CUP','Cuban Peso'),(38,'CYP','Cyprus Pound'),(39,'CZK','Czech Republic Koruna'),(40,'DKK','Danish Krone'),(41,'YDD','Democratic Yemeni Dinar'),(42,'DOP','Dominican Peso'),(43,'XCD','East Caribbean Dollar'),(44,'TPE','East Timor Escudo'),(45,'ECS','Ecuador Sucre'),(46,'EGP','Egyptian Pound'),(47,'SVC','El Salvador Colon'),(48,'EEK','Estonian Kroon (EEK)'),(49,'ETB','Ethiopian Birr'),(50,'EUR','Euro'),(51,'FKP','Falkland Islands Pound'),(52,'FJD','Fiji Dollar'),(53,'GMD','Gambian Dalasi'),(54,'GHC','Ghanaian Cedi'),(55,'GIP','Gibraltar Pound'),(56,'XAU','Gold, Ounces'),(57,'GTQ','Guatemalan Quetzal'),(58,'GNF','Guinea Franc'),(59,'GWP','Guinea-Bissau Peso'),(60,'GYD','Guyanan Dollar'),(61,'HTG','Haitian Gourde'),(62,'HNL','Honduran Lempira'),(63,'HKD','Hong Kong Dollar'),(64,'HUF','Hungarian Forint'),(65,'INR','Indian Rupee'),(66,'IDR','Indonesian Rupiah'),(67,'XDR','International Monetary Fund (IMF) Special Drawing '),(68,'IRR','Iranian Rial'),(69,'IQD','Iraqi Dinar'),(70,'IEP','Irish Punt'),(71,'ILS','Israeli Shekel'),(72,'JMD','Jamaican Dollar'),(73,'JPY','Japanese Yen'),(74,'JOD','Jordanian Dinar'),(75,'KHR','Kampuchean (Cambodian) Riel'),(76,'KES','Kenyan Schilling'),(77,'KWD','Kuwaiti Dinar'),(78,'LAK','Lao Kip'),(79,'LBP','Lebanese Pound'),(80,'LSL','Lesotho Loti'),(81,'LRD','Liberian Dollar'),(82,'LYD','Libyan Dinar'),(83,'MOP','Macau Pataca'),(84,'MGF','Malagasy Franc'),(85,'MWK','Malawi Kwacha'),(86,'MYR','Malaysian Ringgit'),(87,'MVR','Maldive Rufiyaa'),(88,'MTL','Maltese Lira'),(89,'MRO','Mauritanian Ouguiya'),(90,'MUR','Mauritius Rupee'),(91,'MXP','Mexican Peso'),(92,'MNT','Mongolian Tugrik'),(93,'MAD','Moroccan Dirham'),(94,'MZM','Mozambique Metical'),(95,'NAD','Namibian Dollar'),(96,'NPR','Nepalese Rupee'),(97,'ANG','Netherlands Antillian Guilder'),(98,'YUD','New Yugoslavia Dinar'),(99,'NZD','New Zealand Dollar'),(100,'NIO','Nicaraguan Cordoba'),(101,'NGN','Nigerian Naira'),(102,'KPW','North Korean Won'),(103,'NOK','Norwegian Kroner'),(104,'OMR','Omani Rial'),(105,'PKR','Pakistan Rupee'),(106,'XPD','Palladium Ounces'),(107,'PAB','Panamanian Balboa'),(108,'PGK','Papua New Guinea Kina'),(109,'PYG','Paraguay Guarani'),(110,'PEN','Peruvian Nuevo Sol'),(111,'PHP','Philippine Peso'),(112,'XPT','Platinum, Ounces'),(113,'PLN','Polish Zloty'),(114,'QAR','Qatari Rial'),(115,'RON','Romanian Leu'),(116,'RUB','Russian Ruble'),(117,'RWF','Rwanda Franc'),(118,'WST','Samoan Tala'),(119,'STD','Sao Tome and Principe Dobra'),(120,'SAR','Saudi Arabian Riyal'),(121,'SCR','Seychelles Rupee'),(122,'SLL','Sierra Leone Leone'),(123,'XAG','Silver, Ounces'),(124,'SGD','Singapore Dollar'),(125,'SKK','Slovak Koruna'),(126,'SBD','Solomon Islands Dollar'),(127,'SOS','Somali Schilling'),(128,'ZAR','South African Rand'),(129,'LKR','Sri Lanka Rupee'),(130,'SHP','St. Helena Pound'),(131,'SDP','Sudanese Pound'),(132,'SRG','Suriname Guilder'),(133,'SZL','Swaziland Lilangeni'),(134,'SEK','Swedish Krona'),(135,'CHF','Swiss Franc'),(136,'SYP','Syrian Potmd'),(137,'TWD','Taiwan Dollar'),(138,'TZS','Tanzanian Schilling'),(139,'THB','Thai Baht'),(140,'TOP','Tongan Paanga'),(141,'TTD','Trinidad and Tobago Dollar'),(142,'TND','Tunisian Dinar'),(143,'TRY','Turkish Lira'),(144,'UGX','Uganda Shilling'),(145,'AED','United Arab Emirates Dirham'),(146,'UYU','Uruguayan Peso'),(147,'USD','US Dollar'),(148,'VUV','Vanuatu Vatu'),(149,'VEF','Venezualan Bolivar'),(150,'VND','Vietnamese Dong'),(151,'YER','Yemeni Rial'),(152,'CNY','Yuan (Chinese) Renminbi'),(153,'ZRZ','Zaire Zaire'),(154,'ZMK','Zambian Kwacha'),(155,'ZWD','Zimbabwe Dollar');
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resetpass`
--

DROP TABLE IF EXISTS `resetpass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resetpass` (
  `id_reset` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(50) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_reset`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resetpass`
--

LOCK TABLES `resetpass` WRITE;
/*!40000 ALTER TABLE `resetpass` DISABLE KEYS */;
INSERT INTO `resetpass` VALUES (9,'d6fdbc2fa0d5661f47f6de19614bed21b0362ada','athm.kh@gmail.com');
/*!40000 ALTER TABLE `resetpass` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `travel`
--

DROP TABLE IF EXISTS `travel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `travel` (
  `id_travel` mediumint(9) NOT NULL AUTO_INCREMENT,
  `id_from` smallint(6) NOT NULL,
  `departure_date` varchar(10) NOT NULL,
  `departure_time` varchar(50) NOT NULL,
  `id_to` smallint(6) NOT NULL,
  `arrival_date` varchar(50) NOT NULL,
  `arrival_time` varchar(50) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `id_carriage` mediumint(9) NOT NULL,
  `id_user` mediumint(6) NOT NULL,
  PRIMARY KEY (`id_travel`),
  KEY `from` (`id_from`,`id_to`,`id_carriage`),
  KEY `to` (`id_to`),
  KEY `id_carriage` (`id_carriage`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `travel`
--

LOCK TABLES `travel` WRITE;
/*!40000 ALTER TABLE `travel` DISABLE KEYS */;
INSERT INTO `travel` VALUES (70,437,'14/04/2016','21:05',174,'15/04/2016','12:05',0,80,42),(71,417,'16/04/2016','07:10',120,'16/04/2016','12:30',0,81,42),(72,12,'19/04/2016','15:45',205,'19/04/2016','20:50',0,82,42),(73,205,'20/04/2016','08:50',113,'20/04/2016','13:30',0,83,42),(74,113,'11/06/2016','17:00',12,'12/06/2016','08:30',0,84,49),(75,206,'16/06/2016','10:00',168,'16/06/2016','13:05',0,85,42),(76,168,'18/06/2016','14:40',206,'18/06/2016','19:40',0,86,42);
/*!40000 ALTER TABLE `travel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id_type` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name_type` varchar(50) NOT NULL,
  `name_tr_type` varchar(45) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (1,'Document','Document'),(2,'Médicament','Medication'),(3,'Aliment','Food'),(4,'Livre','Book'),(5,'Vêtement','Clothing'),(6,'Appareil électronique','Electronic device'),(7,'Valise','Bag'),(8,'Autre','other');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id_user` mediumint(9) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (42,'Djamel','Khelladi','  ','0033611264890','kh.djamel5553@gmail.com','$1$H1vfzu2e$lPI.n6Ev.MgadDZ8JRu851'),(47,'Athmaneo','braw','sqsqsqsqs  ','121121','athm.kh@gmail.com','$1$7GGzNv6O$6Iyop6CMXAD5lw80wuq1H0'),(49,'Apia','2','  ','43976364581','apia.alg@gmail.com','$1$LUry5ct2$aerIMPsb90hhMcMH5KxjY0'),(50,'athm','kh','        test  ','555555','athm.kh@outlook.com','$1$yacZNVyP$Pa7X92DP.UhT9fNaebaRJ1');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_temp`
--

DROP TABLE IF EXISTS `users_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_temp` (
  `id_temp` smallint(6) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(200) NOT NULL,
  `rand_temp` varchar(50) NOT NULL,
  PRIMARY KEY (`id_temp`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_temp`
--

LOCK TABLES `users_temp` WRITE;
/*!40000 ALTER TABLE `users_temp` DISABLE KEYS */;
INSERT INTO `users_temp` VALUES (45,'said','lahlou','apia.agl@gmail.com','$1$WMSj8YK5$DTU6.oJdIjbNSPKBgZQCK0','5141112222','    ','468f3fbcdc95e9fb716baaf20098e79c255e6a91'),(46,'dora','exploratrice','malioniana@yahoo.fr','$1$dElBdRGC$p6GLzf18UgXbCGRQZjEuG1','51467364543','   ','c3981d892d488597870fc4d0798a379893c621cf');
/*!40000 ALTER TABLE `users_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `weight`
--

DROP TABLE IF EXISTS `weight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `weight` (
  `id_weight` int(11) NOT NULL AUTO_INCREMENT,
  `weight` varchar(20) NOT NULL,
  PRIMARY KEY (`id_weight`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `weight`
--

LOCK TABLES `weight` WRITE;
/*!40000 ALTER TABLE `weight` DISABLE KEYS */;
INSERT INTO `weight` VALUES (1,'0-100 g'),(2,'100-250 g'),(3,'250-500g'),(4,'500-1000 g'),(5,'1-2 kg'),(6,'2-5 kg'),(7,'> 5 kg');
/*!40000 ALTER TABLE `weight` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-06  9:51:46
