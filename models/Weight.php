<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "weight".
 *
 * @property integer $id_weight
 * @property string $weight
 */
class Weight extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'weight';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['weight'], 'required'],
            [['weight'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_weight' => Yii::t('app', 'Id Weight'),
            'weight' => Yii::t('app', 'Weight'),
        ];
    }
}
