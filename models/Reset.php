<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type".
 *
 * @property integer $id_type
 * @property string $name_type
 * @property string $name_tr_type
 */
class Reset extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resetpass';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hash', 'email'], 'required'],
            [['email'], 'string', 'max' => 50],
            [['hash'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
 
}
