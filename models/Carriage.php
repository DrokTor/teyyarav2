<?php

namespace app\models;

use Yii;
use app\models\Type;
use app\models\Weight;
use app\models\Currency;

/**
 * This is the model class for table "carriage".
 *
 * @property integer $id_carriage
 * @property integer $id_type
 * @property integer $id_weight
 * @property string $price
 * @property integer $id_currency
 * @property string $comment
 * @property integer $id_user
 */
class Carriage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carriage';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        [['id_type', 'id_weight', 'price', 'id_currency', 'comment', /*'id_user'*/], 'required'],
            [['id_type', 'id_weight', 'id_currency', 'id_user'], 'integer'],
            [['price'], 'string', 'max' => 50],
            [['comment'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_carriage' => Yii::t('app', 'Id Carriage'),
            'id_type' => Yii::t('app', 'Id Type'),
            'id_weight' => Yii::t('app', 'Id Weight'),
            'price' => Yii::t('app', 'Price'),
            'id_currency' => Yii::t('app', 'Id Currency'),
            'comment' => Yii::t('app', 'Comment'),
            'id_user' => Yii::t('app', 'Id User'),
        ];
    }

    public function getType()
    {
      return $this->hasOne(Type::className(),['id_type'=>'id_type']);
    }
    public function getWeight()
    {
      return $this->hasOne(Weight::className(),['id_weight'=>'id_weight']);
    }
    public function getCurrency()
    {
      return $this->hasOne(Currency::className(),['id_currency'=>'id_currency']);
    }

}
