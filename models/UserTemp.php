<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_temp".
 *
 * @property integer $id_temp
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $address
 * @property string $rand_temp
 */
class UserTemp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_temp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        [['first_name', 'last_name', 'email', 'password', 'phone', 'address'/*, 'rand_temp'*/], 'required'],
            [['first_name', 'last_name', 'email'], 'string', 'max' => 30],
            [['password'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 15],
            [['address'], 'string', 'max' => 200],
            [['rand_temp'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_temp' => Yii::t('app', 'Id Temp'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'rand_temp' => Yii::t('app', 'Rand Temp'),
        ];
    }

    public function beforeSave($insert) {
      parent::beforeSave($insert);
    //dd($this);
      $this->rand_temp = Yii::$app->getSecurity()->generateRandomString();
      return true;
    }
}
