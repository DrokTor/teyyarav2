<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type".
 *
 * @property integer $id_type
 * @property string $name_type
 * @property string $name_tr_type
 */
class Type extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_type', 'name_tr_type'], 'required'],
            [['name_type'], 'string', 'max' => 50],
            [['name_tr_type'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_type' => Yii::t('app', 'Id Type'),
            'name_type' => Yii::t('app', 'Name Type'),
            'name_tr_type' => Yii::t('app', 'Name Tr Type'),
        ];
    }
}
