<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "airports".
 *
 * @property integer $id_airport
 * @property integer $id_country
 * @property string $airport
 * @property string $airport_tr
 */
class Airport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'airports';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_country'], 'required'],
            [['id_country'], 'integer'],
            [['airport'], 'string', 'max' => 71],
            [['airport_tr'], 'string', 'max' => 65],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_airport' => Yii::t('app', 'Id Airport'),
            'id_country' => Yii::t('app', 'Id Country'),
            'airport' => Yii::t('app', 'Airport'),
            'airport_tr' => Yii::t('app', 'Airport Tr'),
        ];
    }
}
