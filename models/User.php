<?php

namespace app\models;

use Yii;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    //public $id;
    /*public $username;*/
    //const SCENARIO_LOGIN = 'login';
    const SCENARIO_VALIDATION = 'validation';

    public $password_repeat;
    public $authKey;
    public $accessToken;

    /*private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];*/

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }



    public function scenarios()
    {
       $scenarios = parent::scenarios();
       //$scenarios[self::SCENARIO_LOGIN] = ['username', 'password'];
       $scenarios[self::SCENARIO_VALIDATION] = ['first_name', 'last_name', 'address', 'phone', 'email', 'password'];
       return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'address', 'phone', 'email', 'password', 'password_repeat'], 'required'],
            [['first_name', 'last_name'], 'string', 'max' => 20],
            [['address'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 15],
          [['email'], 'email'/*, 'max' => 30*/],
            [['password'], 'string', 'max' => 255],
            ['password_repeat', 'compare', 'compareAttribute'=>'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => Yii::t('app', 'Id User'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
          ];

    }

    public function beforeSave($insert) {
    parent::beforeSave($insert);
    //dd($this);
      $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
      return true;
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return (self::findOne($id)) ? new static(self::findOne($id)) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user= self::find()->where(['email'=>$username])->one();
        //dd($user);
            if ($user) {
                return new static($user);
            }


        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id_user;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        //$password=  Yii::$app->getSecurity()->generatePasswordHash($password);
        try {

        } catch (Exception $e) {

        }

        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }
}
