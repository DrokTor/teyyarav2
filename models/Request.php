<?php

namespace app\models;

use Yii;
use app\models\Airport;
use app\models\User;
use app\models\Carriage;


/**
 * This is the model class for table "requests".
 *
 * @property integer $id
 * @property integer $from_id
 * @property integer $to_id
 * @property string $arrival_date
 * @property string $description
 * @property integer $user_id
 * @property integer $carriage_id
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_id', 'to_id', 'arrival_date',   'user_id', 'carriage_id'], 'required'],
            [['from_id', 'to_id', 'user_id', 'carriage_id'], 'integer'],
            [['arrival_date'], 'safe'],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'from_id' => Yii::t('app', 'From'),
            'to_id' => Yii::t('app', 'To'),
            'arrival_date' => Yii::t('app', 'Wished arrival date'),
            'description' => Yii::t('app', 'Description'),
            'user_id' => Yii::t('app', 'User'),
            'carriage_id' => Yii::t('app', 'Carriage'),
        ];
    }

    public function getUser()
    {
      return $this->hasOne(User::className(),['id_user'=>'user_id']);
    }
    public function getFrom()
    {
      return $this->hasOne(Airport::className(),['id_airport'=>'from_id']);
    }
    public function getTo()
    {
      return $this->hasOne(Airport::className(),['id_airport'=>'to_id']);
    }
    public function getCarriage()
    {
      return $this->hasOne(Carriage::className(),['id_carriage'=>'carriage_id']);
    }
}
