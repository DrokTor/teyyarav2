<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Travel;

/**
 * OffersSearch represents the model behind the search form about `app\models\Travel`.
 */
class OffersSearch extends Travel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_travel', 'id_from', 'id_to', 'type', 'id_carriage', 'id_user'], 'integer'],
            [['departure_date', 'departure_time', 'arrival_date', 'arrival_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Travel::find()->with('from');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>[
        'pageSize' => 10,
    ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_travel' => $this->id_travel,
            'id_from' => $this->id_from,
            'id_to' => $this->id_to,
            'type' => $this->type,
            'id_carriage' => $this->id_carriage,
            'id_user' => $this->id_user,
        ]);

        $query->andFilterWhere(['like', 'departure_date', $this->departure_date])
            ->andFilterWhere(['like', 'departure_time', $this->departure_time])
            ->andFilterWhere(['like', 'arrival_date', $this->arrival_date])
            ->andFilterWhere(['like', 'arrival_time', $this->arrival_time]);

        return $dataProvider;
    }
}
