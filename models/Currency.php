<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id_currency
 * @property string $iso
 * @property string $name_currency
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iso', 'name_currency'], 'required'],
            [['iso'], 'string', 'max' => 3],
            [['name_currency'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_currency' => Yii::t('app', 'Id Currency'),
            'iso' => Yii::t('app', 'Iso'),
            'name_currency' => Yii::t('app', 'Name Currency'),
        ];
    }
}
