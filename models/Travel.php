<?php

namespace app\models;

use Yii;
use app\models\Airport;
use app\models\Carriage;
use app\models\User;


/**
 * This is the model class for table "travel".
 *
 * @property integer $id_travel
 * @property integer $id_from
 * @property string $departure_date
 * @property string $departure_time
 * @property integer $id_to
 * @property string $arrival_date
 * @property string $arrival_time
 * @property integer $type
 * @property integer $id_carriage
 * @property integer $id_user
 */
class Travel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'travel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_from', 'departure_date', 'departure_time', 'id_to', 'arrival_date', 'arrival_time', /*'type',*/ 'id_carriage', 'id_user'], 'required'],
            [['id_from', 'id_to', 'type', 'id_carriage', 'id_user'], 'integer'],
            [['departure_date'], 'string', 'max' => 10],
            [['departure_time', 'arrival_date', 'arrival_time'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_travel' => Yii::t('app', 'Id Travel'),
            'id_from' => Yii::t('app', 'Id From'),
            'departure_date' => Yii::t('app', 'Departure Date'),
            'departure_time' => Yii::t('app', 'Departure Time'),
            'id_to' => Yii::t('app', 'Id To'),
            'arrival_date' => Yii::t('app', 'Arrival Date'),
            'arrival_time' => Yii::t('app', 'Arrival Time'),
            'type' => Yii::t('app', 'Type'),
            'id_carriage' => Yii::t('app', 'Id Carriage'),
            'id_user' => Yii::t('app', 'Id User'),
        ];
    }

    public function getUser()
    {
      return $this->hasOne(User::className(),['id_user'=>'id_user']);
    }
    public function getFrom()
    {
      return $this->hasOne(Airport::className(),['id_airport'=>'id_from']);
    }
    public function getTo()
    {
      return $this->hasOne(Airport::className(),['id_airport'=>'id_to']);
    }
    public function getCarriage()
    {
      return $this->hasOne(Carriage::className(),['id_carriage'=>'id_carriage']);
    }
}
