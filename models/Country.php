<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property integer $id_country
 * @property string $country
 * @property string $country_tr
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country'], 'string', 'max' => 34],
            [['country_tr'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_country' => Yii::t('app', 'Id Country'),
            'country' => Yii::t('app', 'Country'),
            'country_tr' => Yii::t('app', 'Country Tr'),
        ];
    }
}
